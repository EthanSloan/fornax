#include <Arduino.h>
#include <dht_nonblocking.h>

#define DHT_SENSOR_TYPE DHT_TYPE_11
static const int DHT_SENSOR_PIN = A3;
DHT_nonblocking dht_sensor( DHT_SENSOR_PIN, DHT_SENSOR_TYPE );

static const int swapMetric = A1;

int pinA = 2;
int pinB = 3;
int pinC = 4;
int pinD = 5;
int pinE = 6;
int pinF = 7;
int pinG = 8;
int pinDP = 13;
int D1 = 9;
int D2 = 10;
int D3 = 11;
int D4 = 12;

byte digitOne;
byte digitTwo;
char letter;

char showMetric;

__attribute__((unused)) void setup() {
    Serial.begin(9600);

    pinMode(pinA, OUTPUT);
    pinMode(pinB, OUTPUT);
    pinMode(pinC, OUTPUT);
    pinMode(pinD, OUTPUT);
    pinMode(pinE, OUTPUT);
    pinMode(pinF, OUTPUT);
    pinMode(pinG, OUTPUT);
    pinMode(pinDP, OUTPUT);
    pinMode(D1, OUTPUT);
    pinMode(D2, OUTPUT);
    pinMode(D3, OUTPUT);
    pinMode(D4, OUTPUT);

    pinMode(swapMetric, INPUT_PULLUP);

    digitOne = -1;
    digitTwo = -1;
    letter = 'a';

    showMetric = 'c';
}

/*
 * Poll for a measurement, keeping the state machine alive.  Returns
 * true if a measurement is available.
 */
static bool measure_environment( float *temperature, float *humidity )
{
    static unsigned long measurement_timestamp = millis( );

    /* Measure once every four seconds. */
    if( millis( ) - measurement_timestamp > 2000ul )
    {
        if(dht_sensor.measure(temperature, humidity))
        {
            measurement_timestamp = millis( );
            return( true );
        }
    }

    return( false );
}

void activeDigit(int digitPosition) {
    digitalWrite(D1, HIGH);
    digitalWrite(D2, HIGH);
    digitalWrite(D3, HIGH);
    digitalWrite(D4, HIGH);

    if(digitPosition == 1) {
        digitalWrite(D1, LOW);
    }
    if(digitPosition == 2) {
        digitalWrite(D2, LOW);
    }
    if(digitPosition == 3) {
        digitalWrite(D3, LOW);
    }
    if(digitPosition == 4) {
        digitalWrite(D4, LOW);
    }
}

void displayDigit(int digit) {
    switch (digit) {
        case 0:
            digitalWrite(pinA, HIGH);
            digitalWrite(pinB, HIGH);
            digitalWrite(pinC, HIGH);
            digitalWrite(pinD, HIGH);
            digitalWrite(pinE, HIGH);
            digitalWrite(pinF, HIGH);
            digitalWrite(pinG, LOW);
            digitalWrite(pinDP, LOW);
            break;
        case 1:
            digitalWrite(pinA, LOW);
            digitalWrite(pinB, HIGH);
            digitalWrite(pinC, HIGH);
            digitalWrite(pinD, LOW);
            digitalWrite(pinE, LOW);
            digitalWrite(pinF, LOW);
            digitalWrite(pinG, LOW);
            digitalWrite(pinDP, LOW);
            break;
        case 2:
            digitalWrite(pinA, HIGH);
            digitalWrite(pinB, HIGH);
            digitalWrite(pinC, LOW);
            digitalWrite(pinD, HIGH);
            digitalWrite(pinE, HIGH);
            digitalWrite(pinF, LOW);
            digitalWrite(pinG, HIGH);
            digitalWrite(pinDP, LOW);
            break;
        case 3:
            digitalWrite(pinA, HIGH);
            digitalWrite(pinB, HIGH);
            digitalWrite(pinC, HIGH);
            digitalWrite(pinD, HIGH);
            digitalWrite(pinE, LOW);
            digitalWrite(pinF, LOW);
            digitalWrite(pinG, HIGH);
            digitalWrite(pinDP, LOW);
            break;
        case 4:
            digitalWrite(pinA, LOW);
            digitalWrite(pinB, HIGH);
            digitalWrite(pinC, HIGH);
            digitalWrite(pinD, LOW);
            digitalWrite(pinE, LOW);
            digitalWrite(pinF, HIGH);
            digitalWrite(pinG, HIGH);
            digitalWrite(pinDP, LOW);
            break;
        case 5:
            digitalWrite(pinA, HIGH);
            digitalWrite(pinB, LOW);
            digitalWrite(pinC, HIGH);
            digitalWrite(pinD, HIGH);
            digitalWrite(pinE, LOW);
            digitalWrite(pinF, HIGH);
            digitalWrite(pinG, HIGH);
            digitalWrite(pinDP, LOW);
            break;
        case 6:
            digitalWrite(pinA, HIGH);
            digitalWrite(pinB, LOW);
            digitalWrite(pinC, HIGH);
            digitalWrite(pinD, HIGH);
            digitalWrite(pinE, HIGH);
            digitalWrite(pinF, HIGH);
            digitalWrite(pinG, HIGH);
            digitalWrite(pinDP, LOW);
            break;
        case 7:
            digitalWrite(pinA, HIGH);
            digitalWrite(pinB, HIGH);
            digitalWrite(pinC, HIGH);
            digitalWrite(pinD, LOW);
            digitalWrite(pinE, LOW);
            digitalWrite(pinF, LOW);
            digitalWrite(pinG, LOW);
            digitalWrite(pinDP, LOW);
            break;
        case 8:
            digitalWrite(pinA, HIGH);
            digitalWrite(pinB, HIGH);
            digitalWrite(pinC, HIGH);
            digitalWrite(pinD, HIGH);
            digitalWrite(pinE, HIGH);
            digitalWrite(pinF, HIGH);
            digitalWrite(pinG, HIGH);
            digitalWrite(pinDP, LOW);
            break;
        case 9:
            digitalWrite(pinA, HIGH);
            digitalWrite(pinB, HIGH);
            digitalWrite(pinC, HIGH);
            digitalWrite(pinD, HIGH);
            digitalWrite(pinE, LOW);
            digitalWrite(pinF, HIGH);
            digitalWrite(pinG, HIGH);
            digitalWrite(pinDP, LOW);
            break;
        default:
            digitalWrite(pinA, LOW);
            digitalWrite(pinB, LOW);
            digitalWrite(pinC, LOW);
            digitalWrite(pinD, LOW);
            digitalWrite(pinE, LOW);
            digitalWrite(pinF, LOW);
            digitalWrite(pinG, LOW);
            digitalWrite(pinDP, LOW);

    }
}

void displayChar(char character) {

    if(character == 'c') {
        digitalWrite(pinA, HIGH);
        digitalWrite(pinB, LOW);
        digitalWrite(pinC, LOW);
        digitalWrite(pinD, HIGH);
        digitalWrite(pinE, HIGH);
        digitalWrite(pinF, HIGH);
        digitalWrite(pinG, LOW);
        digitalWrite(pinDP, LOW);
    }
    else if(character == 'd') {
        digitalWrite(pinA, HIGH);
        digitalWrite(pinB, HIGH);
        digitalWrite(pinC, LOW);
        digitalWrite(pinD, LOW);
        digitalWrite(pinE, LOW);
        digitalWrite(pinF, HIGH);
        digitalWrite(pinG, HIGH);
        digitalWrite(pinDP, LOW);
    }
    else if(character == 'f') {
        digitalWrite(pinA, HIGH);
        digitalWrite(pinB, LOW);
        digitalWrite(pinC, LOW);
        digitalWrite(pinD, LOW);
        digitalWrite(pinE, HIGH);
        digitalWrite(pinF, HIGH);
        digitalWrite(pinG, HIGH);
        digitalWrite(pinDP, LOW);
    }
    else if(character == 'h') {
        digitalWrite(pinA, LOW);
        digitalWrite(pinB, HIGH);
        digitalWrite(pinC, HIGH);
        digitalWrite(pinD, LOW);
        digitalWrite(pinE, HIGH);
        digitalWrite(pinF, HIGH);
        digitalWrite(pinG, HIGH);
        digitalWrite(pinDP, LOW);
    }
    else {
        digitalWrite(pinA, LOW);
        digitalWrite(pinB, LOW);
        digitalWrite(pinC, LOW);
        digitalWrite(pinD, LOW);
        digitalWrite(pinE, LOW);
        digitalWrite(pinF, LOW);
        digitalWrite(pinG, LOW);
        digitalWrite(pinDP, LOW);
    }

}

__attribute__((unused)) void loop() {
    float temperature;
    float humidity;

    /* Measure temperature and humidity.  If the functions returns
    true, then a measurement is available. */
    if(measure_environment(&temperature, &humidity))
    {
        Serial.print( "T = " );
        Serial.print( temperature, 1 );
        Serial.print( " deg. C, H = " );
        Serial.print( humidity, 1 );
        Serial.println( "%" );
        Serial.println(temperature);

        if (showMetric == 'c') {
            digitOne = ((int)temperature / 10) % 10;
            digitTwo = (int)temperature % 10;
            letter = 'c';
        }
        else if (showMetric == 'f') {
            temperature = (double)temperature * 1.8 + 32;
            digitOne = ((int)temperature / 10) % 10;
            digitTwo = (int)temperature % 10;
            letter = 'f';
        }
        else if (showMetric == 'h') {
            digitOne = ((int)humidity / 10) % 10;
            digitTwo = (int)humidity % 10;
            letter = 'h';
        }
    }

    activeDigit(1);
    displayDigit(digitOne);
    delay(2);

    activeDigit(2);
    displayDigit(digitTwo);
    delay(2);

    if (letter == 'c' || letter == 'f') {
        activeDigit(3);
        displayChar('d');
        delay(2);
    }

    activeDigit(4);
    displayChar(letter);
    delay(2);

    if (digitalRead(swapMetric) == LOW) {
        if (showMetric == 'c') {
            showMetric = 'f';
        }
        else if (showMetric == 'f') {
            showMetric = 'h';
        }
        else if (showMetric == 'h') {
            showMetric = 'c';
        }
        delay(1000);
    }
}